#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate rocket_contrib;
extern crate rex;
extern crate base64;
extern crate nix;
extern crate tempfile;

use std::error::Error;
use rex::render::{RenderSettings, svg::SVGRenderer};
use rex::{layout::Style, font::FontUnit, Renderer};
use rocket::request::{Form, Request};
use rocket::http::{Status, ContentType};
use rocket::response::{self, Responder, Response};
use rocket::fairing::AdHoc;
use rocket_contrib::serve::StaticFiles;
use std::io::{Cursor, Read, Write};

#[derive(FromForm)]
struct Input {
    tex: String,
    size: u16
}

enum Format {
    Auto,
    SVG,
    PNG
}
struct Image {
    svg_data: Vec<u8>,
    format: Format
}

const FONT_HOST: &'static str = "https://rynx.org";

fn svg_to_png(svg: &[u8]) -> Result<Vec<u8>, Box<Error>> {
    use std::process::Command;
    use tempfile::Builder;
    let mut input = Builder::new()
        .suffix(".svg")
        .tempfile()?;
    let mut output = Builder::new()
        .suffix(".png")
        .tempfile()?;
    input.write_all(svg)?;
    
    let status = Command::new("inkscape")
        .arg("-z")
        .arg("-d")
        .arg("100")
        .arg("-e")
        .arg(output.path())
        .arg(input.path())
        .status()?;
    if !status.success() {
        return Err("inkscape failed".into());
    }
    let mut png_data = Vec::new();
    output.as_file().read_to_end(&mut png_data)?;
    Ok(png_data)
}

impl<'r> Responder<'r> for Image {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        use rocket::http::hyper::header::{CacheControl, CacheDirective, AccessControlAllowOrigin};
        let mut r = Response::build();
        match self.format {
            Format::SVG | Format::Auto => {
                r.sized_body(Cursor::new(self.svg_data))
                .header(AccessControlAllowOrigin::Value(FONT_HOST.into()))
                .header(ContentType::new("image", "svg+xml"));
            },
            Format::PNG => {
                let png_data = svg_to_png(&self.svg_data).map_err(|_| Status::InternalServerError)?; 
                r.sized_body(Cursor::new(png_data))
                .header(ContentType::new("image", "png"));
            }
        }
        r.header(CacheControl(vec![
            CacheDirective::Public,
            CacheDirective::MaxAge(604800)
        ]))
        .ok()
    }
}


fn render_tex(tex: &str, size: u16, format: Format) -> Result<Image, Box<Error>> {
    let settings = RenderSettings {
        font_size: size,
        font_src: format!("{}/rex/xits.otf", FONT_HOST),
        horz_padding: FontUnit::from(250),
        vert_padding: FontUnit::from(100),
        strict: true,
        style: Style::Display,
        debug: false,
    };
    let svg = SVGRenderer::new(&settings);
    Ok(Image {
        svg_data: svg.render(tex)?,
        format
    })
}

#[get("/render?<tex>&<size>&<format>")]
fn render(tex: String, size: u16, format: Option<String>) -> Result<Image, Box<Error>> {
    let base64_config = base64::Config::new(base64::CharacterSet::UrlSafe, false);
    let input = String::from_utf8(base64::decode_config(&tex, base64_config)?)?;
    let format = match format.as_ref().map(|s| s.as_str()) {
        Some("svg") => Format::SVG,
        Some("png") => Format::PNG,
        _ => Format::Auto
    };
    render_tex(&input, size, format)
}

#[post("/render", data = "<input>")]
fn render_form(input: Form<Input>) -> Result<Image, Box<Error>> {
    render_tex(&input.tex, input.size, Format::Auto)
}

fn main() {
    rocket::ignite()
        .mount("/rex", routes![render, render_form])
        .mount("/rex", StaticFiles::from("data"))
        //.mount("/", StaticFiles::from("/var/www/rynx.org"))
        //.mount("/sebk", StaticFiles::from("/home/sebk/public_html"))
        .attach(AdHoc::on_launch("set uuid", |_| {
            use nix::unistd::{setuid, setgid, getuid};
            if getuid().is_root() { // we are ROOT. not good.
                panic!("running as ROOT");
            }
        }))
        .launch();
}
